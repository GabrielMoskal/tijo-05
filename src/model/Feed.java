package model;

import java.util.ArrayList;
import java.util.List;

/*
 * Stores an RSS feed
 */
public class Feed {

    final String title;
    final String description;

    final List<FeedMessage> entries = new ArrayList<FeedMessage>();

    public Feed(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public List<FeedMessage> getMessages() {
        return entries;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "";
    }

}