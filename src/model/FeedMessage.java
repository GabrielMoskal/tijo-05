package model;

public class FeedMessage {

    String title;
    String description;

    /*
     * Represents one RSS message
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {

        String text = makeLinesFromLongText(removeHtml(description));

        removeHtml(description);

        return makeMessage("Tytuł: " + removeHtml(title))
                + makeMessage("Opis: " + text);

    }

    private String removeHtml(String text) {
        return text.replaceAll("\\<.*?>","");
    }

    private String makeLinesFromLongText(String text) {
        StringBuilder sb = new StringBuilder(text);

        int i = 0;

        while ((i = sb.indexOf(" ", i + 80)) != -1) {
            sb.replace(i, i + 1, "\n");
        }

        return sb.toString();
    }

    private String makeMessage(String msg) {
        return msg + "\n-----------------------------------------------------------\n";
    }

}